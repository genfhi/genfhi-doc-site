---
sidebar_position: 2
---

# Variables

Variables in GenFHI are how you reference actions, components and derive new values.

## Actions

Whenever you create an action it autogenerates an id `action[unique-identifier]`.  
You can use this identifier to pull in the actions values in other parts of your apps.

For example say we've created an action and renamed it `getSDs`.
This action will automatically trigger to fetch StructureDefinition resources.
We can reference it from the list property `data` using a fhirpath expression
`%getSDs.response`

### Action Properties

| Property | Description                                                     |
| -------- | --------------------------------------------------------------- |
| type     | The type of response, e.g. update vs search                       |
| response | The response (if response is search this is a list of resources |
| level    | The level of the response (system vs type vs instance           |

## Components

GenFHI supports several different component types (go to application editor and open the right menu bar for an up-to-date list).
You can reference component values both within actions expressions and from other components.

For example say we create a textinput and set it's ID to search.
We would like to use the value of this textinput within a Patient search action.
We click Add parameters set our parameter to `name` and set the value to be `{{%search.value}}`.
We've now created a Patient search action that will use our search.value when triggered.

Each component will have a different set of properties you can pull from.
You can explore these properties from within the editor. A few key properties are:

| Property   | Type | Description                              |
| ---------- | ---- | ---------------------------------------- |
| selected   | list | The selected row in a list               |
| value      | \*   | The current value of the widget          |
| x          | \*   | The current x position of the widget     |
| y          | \*   | The current y position of the widget     |
| isDisabled | \*   | Whether the component is disabled or not |
