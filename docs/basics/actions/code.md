---
sidebar_position: 2
---

# Custom Code execution

Another action type is the ability to execute custom javascript code.

## Accessing data
To access data you can use application/x-fhir-query and reference widgets or other actions as fhirpath variables for example:

```js
const age = {{%age.value.first()}}
const gender = {{%gender.value.first()}}
const creatine = {{%creatine.value.first()}}
const height = {{%height.value.first()}}
const weight = {{%weight.value.first()}}

const baselineCreatine = ((140 - age)*weight)/(72*creatine)

if(gender==="female"){
  return baselineCreatine * 0.85
} else {
  return baselineCreatine;
}
```
In this example we are pulling in data from several variables with the following ids age, gender, creatine, height, weight.
These variables could be anything from widgets, to other actions (including other custom code actions).
We than access these variables based on their assigend javascript variable name.

## Unique Functions
The following are unqiue functions you can use within a custom code action.

| Property      | Parameters                                  | Description                                 |
|---------------|---------------------------------------------|---------------------------------------------|
| triggerAction | action-id:string                            | Trigger an action with the given id.        |
| modifyState   | variable:string, key:string, value:anything | Set a variables property to the given value |

### Examples

The following will trigger a query with an id of `query1`
```js
triggerAction("query1")
```

The following will trigger a query with an id of `query1`:
```js
triggerAction("query1")
```

The following will set the background red and color purple for a text component with id `text54773`:
```js
modifyState("text54773", "style", "background:red;color:purple");
```

The following will set the value to 5 for an integer widget with id `number1`:
```js
modifyState("number1", "value", 5);
```


## Returning data

After we've run a calculation (in this example we're calculating creatine clearance) we return the value.
Other widgets or actions can than access the output of this action by our actions id. 

For example if we name our id mynewaction we would access the calcuation as follows:
```
%mynewaction.response

