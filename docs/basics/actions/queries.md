---
sidebar_position: 1
---

# FHIR Queries

Queries are how you communicate with external FHIR servers.
With GenFHI you communicate with FHIR servers through FHIR Endpoint resource.

## FHIR Endpoint Resource

You can set endpoints to FHIR servers via the FHIR [Endpoint resource](https://www.hl7.org/fhir/endpoint.html).
This endpoint will define the address of your FHIR server as well as the headers every request will have.
GenFHI has an endpoint editor that you can use to reference and create FHIR Endpoints. From the homescreen of your workspace you can select the Endpoint editor.
You can then reference this endpoint via the action editor `Endpoint` property. We support the use of multiple endpoints within a single application.


## Endpoint Authentication methods

### SMART on FHIR

GenFHI supports [SMART on FHIR](https://docs.smarthealthit.org/) based applications.
Within the endpoint editor you have two options for SMART authentication see below.

### Standalone launch

For standalone launch select `SMART Standalone Launch` for your endpoints Authorization method. 
You will than be presented with a list of options to fill including client id and iss.

### EHR based launch

For SMART ehr launch select `SMART EHR Launch` for your endpoints Authorization method. 
Filling in an ISS here will allow you to get capability completions within the application editor.
But will be ignored in production (for EHR launches ISS will be passed in as a query parameter).


### Accessing Endpoint information from editor
For endpoints we allow you to access meta information (such as smart context) from within an app.
To do this use the `%endpoint` variable. Endpoint information will be a property on this map (with
the key being the endpoints id).
For example if if I have a smart endpoint I am using with id 11b26215-1b2d-4535-b972-96e5d08f830e
to access the smart patient context I would run the following query

```{{%endpoint.`11b26215-1b2d-4535-b972-96e5d08f830e`.patient.id}}```.


The shape of data within a smart endpoint is as follows:
```js
{
  "clientId": null, // <- the clientID of your application
  "encounter": {
    "id": null  // <- the logged in encounter id.
  },
  "patient": {
    "id": null // <- the logged in patient id.
  },
  "user": {
    "id": null, // <- the logged in user id.
    "resourceType": null // <- the logged in user resource type.
  }
}
```
