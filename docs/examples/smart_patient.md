---
sidebar_position: 1
---

# SMART on FHIR Patient app

For this application we will be creating a SMART on FHIR standalone application.
This app will allow a Patient to login and view their Encounter and Procedure history.

## Steps

### 1. Open Endpoint Editor

### 2. Add smart information
Name the endpoint SMART-HEALTHIT.

We’ll now add some smart information. We will build an app that does standalone patient launch.
Set the authorization method to be SMART standalone launch. Then set the following SMART paramers:

client id:patient-app <br />
scopes: patient launch offline_access openid fhirUser <br />
iss: https://launch.smarthealthit.org/v/r4/sim/eyJrIjoiMSIsImoiOiIxIn0/fhir 



### 3. Go back to root and open Client Application editor
Hit create new app.

### 4. Open Components Tab
We have various components you can use to create your applications we will be using the list component for now.

### 5.  Drag and Drop list component
We’ll rename this component to procedurelist

### 6. Add columns to the list
For most values and properties on items we use fhirpath or x-fhir-query. Add the following columns with corresponding expressions.

**Category** `$this.category` ← Classification of the procedure <br/>
**Status** `$this.status` ← We’ll include status which includes information like whether a procedure is in-progress | not-done | on-hold or completed. <br/>
**Reason** `$this.reasonCode` ← We’ll incude information about why the procedure is being performed <br/>
**Performated At** `$this.performed` ← And information about when the procedure has been performed. <br/>

### 7. Drag and Drop List component
We’ll add another list widget and rename it to encounterList

### 8. Add columns to the list
**Id** `$this.id` ← We’ll include the id for the Encounter resource <br/>
**Status** `$this.status` ← We’ll add the current status of the Encounter resource. status will tell you whether an encounter is planned, arrived or onleave <br/>
**Type** `$this.type` ← We’ll include the Encounter type <br/>
**Start** `$this.period.start` ←  And the start and ending times of the encounter <br/>
**End** `$this.period.end`


### 9. Create actions
We’ll now create a few actions to query for Procedure and Encounter resources from our smart launch.
For each action associate it with the Endpoint we created from step2 `SMART-HEALTHIT`.

Below is the list of parameter names and expression values
#### Procedure
id → patientsProcedures <br/>
Resource -> Procedure 
##### Parameters:
patient → `{{%endpoint.[your-endpoint-id].patient.id}}`

#### Encounter
id → patientsEncounters  <br/>
Resource -> Encounter 

#### Parameters:
patient → `{{%endpoint.[your-endpoint-id].patient.id}}`

#### smartpatient
id -> smartptient <br />
Location -> `Patient/{{%endpoint.[your-endpoint-id].patient.id}}` <br />


### 8. Create HTML Component
Drag to the top an HTML component that we will use as a header.
We'll reference our smartpatient action to pull in our logged in patients name. 
When the name is not present default to "Patient".
```html
<h1 style="text-align:center;">
{{%smartpatient.response.name.given[0] or "Patient"}}
Encounter and Procedure History
</h1>
```

### 10. Tie our procedure and encounter 
Click the procedure list we created in step 4. In item properties set it's data to be `%patientsProcedures.response` <br/>
Click the encounter list we created in step 5. In item properties set it's data to be `%patientsEncounters .response`.

### 11. Publish App 
For publishing we have two methods one method is you can run an appserver docker image on your infrastructure where you provide it with application manifests from the file system.
The other method is to download a standalone docker image of your application. 
For this example we will download and run a standalone docker image.

Click publish and select docker than `Generate Image` wait for image to be generated and download tar. Follow steps on screen to load and run docker image.


### 12. Test Application
Once application is running we should be presented with login screen use the following for a username `87a339d0-8cae-418e-89c7-8651e6aab3c6` and click login.
We should now be presented with information about our logged in patient in addtion to their Procedure and Encounter history.
