

# Support

List of properties and parts of SDC spec supported. 


## Questionnare Properties Supported

List of properties our renderer currently supports

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Property</th>
<th scope="col" class="org-left">Supported?</th>
<th scope="col" class="org-left">Note</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">linkId</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">definition</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">code</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">prefix</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">text</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">type</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">enableWhen</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">enableBehavior</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">required</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">repeats</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">readOnly</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">maxLength</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">answerValueSet</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">answerOption</td>
<td class="org-left">Yes</td>
<td class="org-left">Currently only supported for codes</td>
</tr>


<tr>
<td class="org-left">initial</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>


## SDC Form Behavior


### Calculations

Support for SDC form behavior calculations <http://build.fhir.org/ig/HL7/sdc/behavior.html#calculations>
We do not support cqf expressions in general. 

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">Code</td>
<td class="org-left">Supported?</td>
<td class="org-left">Description</td>
</tr>


<tr>
<td class="org-left">cqf-library</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">launchContext</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">variable</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">initialExpression</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">calculatedExpression</td>
<td class="org-left">Yes</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">cqf-calculatedValue</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">cqf-expression</td>
<td class="org-left">No</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>

