---
id: intro
slug: /
sidebar_position: 1
---

# What does GenFHI do?

GenFHI is low-code tooling for building FHIR based applications.

## Getting Started

### Signup
Get started by **[signing up for a new workspace](https://workspace.genfhi.app)**.

### Basics 
Basics will give a brief overview of how to create queries and tie them to components.

### Run through the example app
Create the example [application](examples/smart_patient)

## Support
Email at dev@genfhi.app for support inquiries.
