
# List
## Properties
| Label | ID | Type |
| ---- | ---- | --- |
| Label | label | string |
| Label Positioning | labelPositioning | code |
| Data | value | json |    