
# Decimal
## Properties
| Label | ID | Type |
| ---- | ---- | --- |
| Label | label | string |
| Label Positioning | labelPositioning | code |
| Read-only? | isDisabled | boolean |
|  | type | string |
|  | value | number |
|  | value | decimal |    