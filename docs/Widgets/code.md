
# Code
## Properties
| Label | ID | Type |
| ---- | ---- | --- |
| Label | label | string |
| Label Positioning | labelPositioning | code |
| Read-only? | isDisabled | boolean |
|  | value | string |    