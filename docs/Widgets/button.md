
# Button
## Properties
| Label | ID | Type |
| ---- | ---- | --- |
| Type | type | code |
|  | className | string |
| Label | label | string |
| Read-only? | isDisabled | boolean |
| Style | style | string |    