
# Json
## Properties
| Label | ID | Type |
| ---- | ---- | --- |
| Label | label | string |
| Label Positioning | labelPositioning | code |
| Read-only? | disabled? | boolean |
| Placeholder | placeholder-string | string |
| Value | value | string |    