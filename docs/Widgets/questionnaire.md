
# Questionnaire
## Properties
| Label | ID | Type |
| ---- | ---- | --- |
| Label | label | string |
| Label Positioning | labelPositioning | code |
| Questionnaire | questionnaire | json |
| Questionnaire Response | questionnaireResponse | json |    