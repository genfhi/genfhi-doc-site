const fs = require("fs");

const widgetData = JSON.parse(fs.readFileSync("./data/widgets.json", "utf8"));

function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

function deriveTableProperties(widgetData) {
  const header = `| Label | ID | Type |
| ---- | ---- | --- |`;
  return widgetData.properties.reduce(
    (doc, property) =>
      `${doc}
| ${
  property["editor-props"] ? property["editor-props"].label : ""
} | ${property.name} | ${property.type} |`,
    header
  );
}


Object.keys(widgetData)
  .sort()
  .map((key) => {
    const widgetMetaInformation = widgetData[key];

    const documentation = `
# ${capitalize(key)}
## Properties
${deriveTableProperties(widgetMetaInformation)}    `;

    fs.writeFileSync(`docs/Widgets/${key}.md`, documentation);
  });
