import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";
import dndImg from "@site/static/img/dnd.png";
import fhirpathImg from "@site/static/img/fhirpath.png";
import publishingImg from "@site/static/img/publishing.png";

const FeatureList = [
  {
    title: "Drag and Drop",
    Img: dndImg,
    description: (
      <>
        GenFHI was designed to be an easy to use. Drag and drop widgets to build
        applications.
      </>
    ),
  },
  {
    title: "FHIR Native Support",
    Img: fhirpathImg,
    description: (
      <>
        GenFHI is built with FHIR in mind. Use FHIRPath expressions and have
        automatic completion basedon your fhir endpoints capabilities.
      </>
    ),
  },
  {
    title: "Publish your app",
    Img: publishingImg,
    description: (
      <>
        To publish you can create standalone docker images of your app, download
        application manifest and deploy on existing running image or generate a
        shareable url to access your app.
      </>
    ),
  },
];

function Feature({ Img, Svg, title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        {Svg ? (
          <Svg className={styles.featureSvg} role="img" />
        ) : (
          <img className={styles.featureSvg} src={Img} />
        )}
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
